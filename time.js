const {Builder, By, Key, until} = require('selenium-webdriver');

(async function example() {
  let driver = await new Builder().forBrowser('firefox').build();
  try {
    await driver.get('https://www.google.com/');

    // Accept the Google GDPR
    await driver.findElement(By.id('L2AGLb')).sendKeys('webdriver', Key.ENTER);

    // Insert search term and search
    await driver.findElement(By.className('gLFyf gsfi')).sendKeys('time', Key.ENTER);

    // Wait for time to show up on page
    let element = By.className('gsrt vk_bk FzvWSb YwPhnf');
    await driver.wait(until.elementLocated(element, 10000));

    // Save found time on variable and print it
    const time = await driver.findElement(By.className("gsrt vk_bk FzvWSb YwPhnf")).getAttribute('innerText');
    console.log(`Current time is: ${time}`);

  } finally {
    await driver.quit();
  }
})();